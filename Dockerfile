FROM microsoft/dotnet:2.1-sdk AS build

WORKDIR /app
# copy csproj and restore as distinct layers
COPY *.sln .
COPY src/SampleApp.Core/*.csproj ./src/SampleApp.Core/
COPY src/SampleApp.Infrastructure/*.csproj ./src/SampleApp.Infrastructure/
COPY src/SampleApp.PesentationLayer/*.csproj ./src/SampleApp.PesentationLayer/
COPY src/SampleApp.SeviceLayer/*.csproj ./src/SampleApp.SeviceLayer/
COPY src/SampleApp.Web/*.csproj ./src/SampleApp.Web/
COPY tests/SampleApp.Tests/*.csproj ./tests/SampleApp.Tests/
RUN dotnet restore

# copy and build everything else
COPY src/SampleApp.Core/. ./src/SampleApp.Core/
COPY src/SampleApp.Infrastructure/. ./src/SampleApp.Infrastructure/
COPY src/SampleApp.PesentationLayer/. ./src/SampleApp.PesentationLayer/
COPY src/SampleApp.SeviceLayer/. ./src/SampleApp.SeviceLayer/
COPY src/SampleApp.Web/. ./src/SampleApp.Web/
COPY tests/SampleApp.Tests/. ./tests/SampleApp.Tests/

RUN dotnet build

FROM build AS testrunner
WORKDIR /app/tests/SampleApp.Tests
ENTRYPOINT ["dotnet", "test","--logger:trx"]

FROM build AS test
WORKDIR /app/tests/SampleApp.Tests
RUN dotnet test

FROM test AS publish
#FROM build AS publish
WORKDIR /app/src/SampleApp.Web
RUN dotnet publish -o out

# Build runtime image
FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=publish /app/src/SampleApp.Web/out ./
ENTRYPOINT ["dotnet", "SampleApp.Web.dll"]