﻿using SampleApp.Core.Entities;
using SampleApp.Core.Interfaces;

using System.Linq;
using System.Collections.Generic;

namespace SampleApp.Core
{
    public static class DatabasePopulator
    {
        public static int PopulateDatabase(IRepository<Invoice> todoRepository)
        {
            if (todoRepository.List().Any()) return 0;

            todoRepository.Add(
                new Invoice()
                    {
                        Items = new List<Item>
                                    {
                                        new Item { Title = "Item1", Price = 1, Quantity = 10 },
                                        new Item { Title = "Item1 Fix", Price = 1, Quantity = 2 },
                                    }
                    });
            todoRepository.Add(
                new Invoice()
                    {
                        Items = new List<Item>
                                    {
                                        new Item
                                            {
                                                Title = "Bugs introduce",
                                                Price = 1,
                                                Quantity = 100
                                            },
                                        new Item { Title = "Bugs Fix", Price = 1, Quantity = 4 },
                                    }
                    });
            todoRepository.Add(
                new Invoice()
                    {
                        Items = new List<Item>
                                    {
                                        new Item { Title = "Coffee", Price = 1, Quantity = 18 },
                                        new Item { Title = "Cakes", Price = 10, Quantity = 9 },
                                    }
                    });
            return todoRepository.List().Count;
        }
    }
}