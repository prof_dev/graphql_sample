﻿
namespace SampleApp.Core.Entities
{
    using SampleApp.Core.SharedKernel;
    using System.Collections.Generic;

    using SampleApp.Core.Enums;

    public class Invoice : BaseEntity
    {

        public InvoiceState State { get; set; } = InvoiceState.Created;

        public IList<Item> Items { get; set; }

//        public Customer Customer {get; set; }

//        public void PayInvoice()
//        {
//            State = InvoiceState.Paid;
//        }
    }
}