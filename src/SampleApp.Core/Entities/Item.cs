﻿using SampleApp.Core.SharedKernel;

namespace SampleApp.Core.Entities
{
    using System;

    public class Item : BaseEntity
    {
        public string Title { get; set; }

        public int Quantity { get; set; }

        public int Price { get; set; }

        public Guid InvoiceId { get; set; }

        public Invoice Invoice { get; set; }
    }
}