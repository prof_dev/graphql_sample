﻿namespace SampleApp.Core.Enums
{
    public enum InvoiceState
    {
        Created,
        Paid,
        Expired,
        PartlyPaid,
    }
}