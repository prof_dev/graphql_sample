﻿namespace SampleApp.Core.Exceptions
{
    using System;

    using SampleApp.Core.Enums;

    public class InvoiceNotFoundException : Exception
    {
        private Guid Id;

        public InvoiceNotFoundException(Guid Id)
        {
            this.Id = Id;
        }

        public override string Message
        {
            get
            {
                return $"There id no invoice with {Id} in db";
            }
        }
    }
}