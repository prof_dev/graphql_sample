﻿namespace SampleApp.Core.Exceptions
{
    using System;

    using SampleApp.Core.Enums;

    public class WrongInvoiceStateException:Exception
    {
        private InvoiceState state;
        private Guid Id;
        private string reason;

        public WrongInvoiceStateException(InvoiceState state, Guid Id, string reason)
        {
            this.state = state;
            this.Id = Id;
            this.reason = reason;
        }

        public override string Message
        {
            get
            {
                return $"For invoice {Id} unexcpected state {state}. Reason {reason}";
            }
        }
    }
}