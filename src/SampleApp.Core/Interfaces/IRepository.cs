﻿using System.Collections.Generic;
using SampleApp.Core.SharedKernel;

namespace SampleApp.Core.Interfaces
{
    using System;

    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(Guid id);
        List<T> List();
        T Add(T entity, bool isLazy = false);
        void Update(T entity, bool isLazy = false);
        void Delete(T entity, bool isLazy = false);
        void Save();
    }
}