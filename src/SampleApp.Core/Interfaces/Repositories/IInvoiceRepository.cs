﻿namespace SampleApp.Core.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using SampleApp.Core.Entities;
    using SampleApp.Core.Enums;

    public interface IInvoiceRepository : IRepository<Invoice>
    {
        IList<Invoice> GetByState(Func<Invoice,Boolean> condition);
        IList<Invoice> GetByState(InvoiceState state);

        void DeleteById(Guid id);
    }
}