﻿namespace SampleApp.Core.Interfaces.Repositories
{
    using SampleApp.Core.Entities;
    public interface IItemRepository:IRepository<Item>
    {
    }
}