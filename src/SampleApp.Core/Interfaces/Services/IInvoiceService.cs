﻿namespace SampleApp.Core.Interfaces.Services
{
    using System;
    using System.Collections.Generic;

    using SampleApp.Core.Entities;

    public interface IInvoiceService
    {
        IList<Invoice >List();

        Invoice Add(Invoice invoice);

        Invoice Update(Invoice invoice);

        Invoice GetById(Guid id);

        IList<Invoice> GetAllUnpaid();

        void Pay(Guid invoiceId);

        void Delete(Guid id);
    }
}