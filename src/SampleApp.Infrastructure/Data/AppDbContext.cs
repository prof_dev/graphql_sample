﻿using Microsoft.EntityFrameworkCore;

using SampleApp.Core.Entities;

namespace SampleApp.Infrastructure.Data
{
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Item> Items{ get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Invoice>(ConfigureInvoice);
            builder.Entity<Item>(ConfigureItem);

        }

        private void ConfigureItem(EntityTypeBuilder<Item> builder)
        {
            builder.HasIndex(x => x.Id);
            builder.HasOne(x => x.Invoice).WithMany(x=>x.Items).HasForeignKey(x => x.InvoiceId);
        }

        private void ConfigureInvoice(EntityTypeBuilder<Invoice> builder)
        {
            builder.HasIndex(x => x.Id);
        }
    }
    }