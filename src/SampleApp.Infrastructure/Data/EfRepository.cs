using System.Collections.Generic;
using System.Linq;
using SampleApp.Core.Interfaces;
using SampleApp.Core.SharedKernel;
using Microsoft.EntityFrameworkCore;

namespace SampleApp.Infrastructure.Data
{
    using System;

    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _dbContext;

        public EfRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual IQueryable<T> All => _dbContext.Set<T>().AsQueryable();

        public virtual T GetById(Guid id)
        {
            return All.SingleOrDefault(e => e.Id == id);
        }

        public List<T> List()
        {
            return All.ToList();
        }

        public virtual T Add(T entity, bool isLazy = false)
        {
            _dbContext.Set<T>().Add(entity);
            if (!isLazy)
                _dbContext.SaveChanges();

            return entity;
        }

        public void Delete(T entity, bool isLazy = false)
        {
            _dbContext.Set<T>().Remove(entity);
            if (!isLazy) 
                _dbContext.SaveChanges();
        }

        public virtual void Update(T entity, bool isLazy = false)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            if (!isLazy)
                _dbContext.SaveChanges();
        }

        public void Save()
        {
            this._dbContext.SaveChanges();
        }
    }
}