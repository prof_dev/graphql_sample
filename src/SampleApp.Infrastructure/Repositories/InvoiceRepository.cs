﻿namespace SamplApp.DomainLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using SampleApp.Core.Entities;
    using SampleApp.Core.Enums;
    using SampleApp.Core.Exceptions;
    using SampleApp.Core.Interfaces.Repositories;
    using SampleApp.Infrastructure.Data;

    public class InvoiceRepository : EfRepository<Invoice>, IInvoiceRepository
    {
        public InvoiceRepository(AppDbContext dbContext)
            : base(dbContext)
        {
        }

        public override IQueryable<Invoice> All
        {
            get
            {
                return base.All.Include(x=>x.Items);
            }
        }

        public IList<Invoice> GetByState(Func<Invoice,bool> condition)
        {
            return All.Where(x => condition(x)).ToList();
        }

        public IList<Invoice> GetByState(InvoiceState state)
        {
            return All.Where(x => x.State == state).ToList();
        }

        public void DeleteById(Guid id)
        {
            var item = GetById(id);

            // deleting of items should take care migrations.

            if (item == null) throw new InvoiceNotFoundException(id);

            Delete(item);
        }
    }
}