﻿namespace SamplApp.DomainLayer.Repositories
{
    using SampleApp.Core.Entities;
    using SampleApp.Core.Interfaces.Repositories;
    using SampleApp.Infrastructure.Data;

    public class ItemRepository:EfRepository<Item>, IItemRepository
    {
        public ItemRepository(AppDbContext dbContext)
            : base(dbContext)
        {

            
        }
    }
}