﻿namespace SampleApp.PresentationLayer.Api
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;

    using Microsoft.AspNetCore.Http.Extensions;
    using Microsoft.AspNetCore.Mvc;

    using Newtonsoft.Json.Linq;

    using SampleApp.Core.Interfaces.Services;

    using InvoiceDTO = ApiModels.InvoiceDTO;

    public class InvoicesAuthController : Controller
    {
        private readonly IInvoiceService invoiceService;

        private readonly IMapper mapper;

        public InvoicesAuthController(IMapper mapper, IInvoiceService invoiceService)
        {
            this.invoiceService = invoiceService;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult ListAuth()
        {
            var items = this.invoiceService.List();
            return this.Ok(this.mapper.Map<List<InvoiceDTO>>(items));
        }

        [HttpGet]
        [Route("api/auth/invoices")]
        public async Task<IActionResult> ListAuthWithToken()
        {
            using (var client = new HttpClient())
            {
                {
                    client.BaseAddress = new Uri(Request.GetDisplayUrl());
                    var request = new HttpRequestMessage(HttpMethod.Get, "/auth/invoices");

                    dynamic content = new JObject();

                    request.Content = new StringContent(
                        content.ToString(),
                        Encoding.UTF8,
                        "application/json"
                    );

                    // Setup header(s)
                    request.Headers.Add("Authorization", "Barier 000");

                    var response = await client.SendAsync(request);
                    
                    return this.Ok(await response.Content.ReadAsAsync<List<InvoiceDTO>>());
                }
            }
        }
    }
}