﻿namespace SampleApp.PresentationLayer.Api
{
    using System;
    using System.Collections.Generic;

    using AutoMapper;

    using Microsoft.AspNetCore.Mvc;

    using SampleApp.Core.Entities;
    using SampleApp.Core.Interfaces.Services;
    using SampleApp.PresentationLayer.Filters;

    using InvoiceDTO = ApiModels.InvoiceDTO;

    [Route("api/[controller]")]
    [ValidateModel]
    public class InvoicesController : Controller
    {
        private readonly IInvoiceService invoiceService;
        private readonly IMapper mapper;

        public InvoicesController(IMapper mapper, IInvoiceService invoiceService)
        {
            this.invoiceService = invoiceService;
            this.mapper = mapper;
        }

        // GET: api/Invoices
        [HttpGet]
        public IActionResult List()
        {
            var items = this.invoiceService.List();
            return this.Ok(this.mapper.Map<List<InvoiceDTO>>(items));
        }

        // GET: api/Invoices
        [HttpGet("{id:Guid}")]
        public IActionResult GetById(Guid id)
        {
            var item = this.invoiceService.GetById(id);
            return this.Ok(this.mapper.Map<InvoiceDTO>(item));
        }

        // POST: api/Invoices
        [HttpPost]
        public IActionResult Post([FromBody] InvoiceDTO invoice)
        {
            var result = this.invoiceService.Add(this.mapper.Map<Invoice>(invoice));
            return this.Ok(this.mapper.Map<InvoiceDTO>(result));
        }

        [HttpPatch]
        public IActionResult Update([FromBody] InvoiceDTO invoice)
        {
            var result  = this.invoiceService.Update(this.mapper.Map<Invoice>(invoice));
            return this.Ok(this.mapper.Map<InvoiceDTO>(result));
        }

        [HttpDelete("{id:Guid}")]
        public IActionResult Delete(Guid id)
        {
            this.invoiceService.Delete(id);
            return this.Ok();
        }
    }
}
