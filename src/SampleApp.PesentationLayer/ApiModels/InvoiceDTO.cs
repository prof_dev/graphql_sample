﻿namespace SampleApp.PresentationLayer.ApiModels
{
    using System;
    using System.Collections.Generic;

    using SampleApp.Core.Enums;

    public class InvoiceDTO
    {
        public Guid Id { get; set; }

        public InvoiceState State { get; set; }
        public IList<ItemDTO> Items { get; set; }
    }
}
