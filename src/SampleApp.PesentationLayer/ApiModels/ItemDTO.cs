﻿using System;

namespace SampleApp.PresentationLayer.ApiModels
{

    public class ItemDTO
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public int Quantity { get; set; }

        public int Price { get; set; }
    }
}