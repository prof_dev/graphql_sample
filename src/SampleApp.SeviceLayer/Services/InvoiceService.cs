﻿namespace SampleApp.ServiceLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using SampleApp.Core.Entities;
    using SampleApp.Core.Enums;
    using SampleApp.Core.Exceptions;
    using SampleApp.Core.Interfaces.Repositories;
    using SampleApp.Core.Interfaces.Services;

    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceRepository invoiceRepo;
        public readonly IItemRepository itemRepo;
        public readonly IMapper mapper;


        public InvoiceService(IInvoiceRepository invoiceRepo, IItemRepository itemRepo, IMapper mapper)
        {
            this.invoiceRepo = invoiceRepo;
            this.itemRepo = itemRepo;
            this.mapper = mapper;
        }

        public IList<Invoice> List()
        {
            return this.invoiceRepo.List();
        }

        public Invoice Add(Invoice invoice)
        {
            invoice.State = InvoiceState.Created;

            var savedEntrie = invoiceRepo.Add(invoice);

            return savedEntrie;
        }

        public Invoice Update(Invoice invoice)
        {
            var dbEntrie = invoiceRepo.GetById(invoice.Id);

            dbEntrie.State = invoice.State;

            foreach (Item dbItem in dbEntrie.Items)
            {
                // Updating existing
                var newItem = invoice.Items.FirstOrDefault(x => x.Id == dbItem.Id);
                if (newItem != null)
                {
                    dbItem.Price = newItem.Price;
                    dbItem.Quantity = newItem.Quantity;
                    dbItem.Title = newItem.Title;
                    this.itemRepo.Update(dbItem, true);

                    invoice.Items.Remove(newItem);
                }
                else // removing non usable
                {
                    this.itemRepo.Delete(dbItem, true);
                }
            }

            //adding new
            foreach (Item item in invoice.Items)
            {
                item.InvoiceId = dbEntrie.Id;
                this.itemRepo.Add(item, true);
            }
            this.invoiceRepo.Save();
            return dbEntrie;
        }

        public Invoice GetById(Guid id)
        {
            return this.invoiceRepo.GetById(id);
        }

        public IList<Invoice> GetAllUnpaid()
        {
            return this.invoiceRepo.GetByState(x => x.State != InvoiceState.Paid);
        }

        public void Pay(Guid invoiceId)
        {
            var invoice = this.invoiceRepo.GetById(invoiceId);
            if (invoice.State == InvoiceState.Paid)
                throw new WrongInvoiceStateException(invoice.State, invoiceId, "Can't pay already paid invoice.");

            invoice.State = InvoiceState.Paid;

            this.invoiceRepo.Update(invoice);
        }

        public void Delete(Guid id)
        {
            this.invoiceRepo.DeleteById(id);
        }
    }
}
