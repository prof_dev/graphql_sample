﻿namespace SampleApp.Web.Authication
{
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc.Authorization;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class CustomAuthFilter : AuthorizeFilter
    {

        public CustomAuthFilter(AuthorizationPolicy policy):base(policy)
        {
        }

        public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.Request.Path.ToString().StartsWith("/auth"))
            {
                return;
            }

            await base.OnAuthorizationAsync(context);
        }
    }
}