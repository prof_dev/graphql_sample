﻿namespace SampleApp.Web.Authication
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Authentication;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Microsoft.Net.Http.Headers;

    public class CustomAuthHandler : AuthenticationHandler<CustomAuthOptions>
    {
        public CustomAuthHandler(IOptionsMonitor<CustomAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) 
            : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            // Get Authorization header value
            if (!this.Request.Headers.TryGetValue(HeaderNames.Authorization, out var authorization))
            {
                return AuthenticateResult.Fail("Cannot read authorization header.");
            }

            if (!authorization.ToString().ToLowerInvariant().StartsWith("barier")) return AuthenticateResult.Fail("Custom Authorization faill.");

            // Create authenticated user
            var identities = new List<ClaimsIdentity> {new ClaimsIdentity("custom auth type")};
            var ticket = new AuthenticationTicket(new ClaimsPrincipal(identities), this.Options.Scheme);

            return AuthenticateResult.Success(ticket);
        }
    }
}