﻿namespace SampleApp.Web.Authication
{
    using Microsoft.AspNetCore.Authentication;

    public class CustomAuthOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "custom auth";
        public string Scheme => DefaultScheme;
        public string AuthKey { get; set; }
    }
}