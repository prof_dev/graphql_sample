﻿using SampleApp.Core;
using SampleApp.Core.Entities;
using SampleApp.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace SampleApp.Web.Controllers
{
    using System.Collections.Generic;

    using AutoMapper;

    using Microsoft.AspNetCore.Mvc.Rendering;

    using SampleApp.Core.Interfaces.Services;
    using SampleApp.PresentationLayer.ApiModels;
    using SampleApp.Web.Models.Invoice;

    public class InvoiceController : Controller
    {
        private readonly IInvoiceService invoiceService;

        private readonly IMapper mapper;

        public InvoiceController(IInvoiceService invoiceService, IMapper mapper)
        {
            this.invoiceService = invoiceService;
            this.mapper = mapper;
        }

        public IActionResult Index()
        {
            var items = this.invoiceService.List();
            return View(this.mapper.Map<List<InvoiceDTO>>(items));
        }

        public IActionResult ChangeInvoice()
        {
            return Ok();
        }

        public IActionResult GetUnpaid()
        {
            var items = this.invoiceService.GetAllUnpaid();
            return View("Index", this.mapper.Map<List<InvoiceDTO>>(items));
        }

        public IActionResult PayInvoice()
        {
            var items = this.invoiceService.GetAllUnpaid();

            var vm = new PayInvoiceModel();
            vm.Invoices = new SelectList(this.mapper.Map<List<InvoiceDTO>>(items), "Id", "Id");
            return View(vm);
        }

        [HttpPost]
        public IActionResult PayInvoice(PayInvoiceModel payModel)
        {
            this.invoiceService.Pay(payModel.InvoiceId);
            return RedirectToAction("index", "home");
        }
    }
}
