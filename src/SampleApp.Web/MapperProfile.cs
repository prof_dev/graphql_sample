﻿namespace SampleApp.Web
{
    using AutoMapper;

    using SampleApp.Core.Entities;
    using SampleApp.PresentationLayer.ApiModels;

    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<InvoiceDTO, Invoice>()
                .ForMember(dest => dest.Events, opt => opt.Ignore());
            CreateMap<ItemDTO, Item>()
                .ForMember(dest => dest.Events, opt => opt.Ignore())
                .ForMember(dest => dest.InvoiceId, opt => opt.Ignore())
                .ForMember(dest => dest.Invoice, opt => opt.Ignore());
        }
    }
}