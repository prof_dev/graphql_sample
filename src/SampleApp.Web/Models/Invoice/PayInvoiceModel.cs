﻿namespace SampleApp.Web.Models.Invoice
{
    using System;

    using Microsoft.AspNetCore.Mvc.Rendering;

    using SampleApp.PresentationLayer.ApiModels;

    public class PayInvoiceModel
    {
        public Guid InvoiceId{ get; set; }
        public SelectList Invoices {  get; set; }
        public string SelectedInvoicesStr {  get; set; }
        public InvoiceDTO SelectedInvoices {  get; set; }
    }
}