﻿namespace SampleApp.Web
{
    using System;
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;

    using SampleApp.Core.Entities;
    using SampleApp.Core.Enums;
    using SampleApp.Infrastructure.Data;

    public static class SeedData
    {
        public static void PopulateTestData(this IApplicationBuilder app)
        {
            AppDbContext context = app.ApplicationServices.GetService<AppDbContext>();
            PopulateTestData(context);
        }

        public static void PopulateTestData(AppDbContext context) {

            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();

            context.Invoices.Add(new Invoice { Id = id1, State = InvoiceState.Created});
            context.Invoices.Add(new Invoice { Id = id2, State = InvoiceState.Created});
            context.Invoices.Add(new Invoice { Id = id3, State = InvoiceState.Created});
            context.SaveChanges();

            var items = new List<Item>
                            {
                                new Item{ InvoiceId = id1, Title = "Item1", Price = 1, Quantity = 10 },
                                new Item{ InvoiceId = id1, Title = "Item1 Fix", Price = 1, Quantity = 2 },
                                new Item{ InvoiceId = id2, Title = "Bugs introduce", Price = 1, Quantity = 100},
                                new Item{ InvoiceId = id2, Title = "Bugs Fix", Price = 1, Quantity = 4},
                                new Item{ InvoiceId = id3, Title = "Coffee", Price = 1, Quantity = 18},
                                new Item{ InvoiceId = id3, Title = "Cakes", Price = 10, Quantity = 9},
                            };

            foreach (Item item in items)
            {
                context.Items.Add(item);
            }

            context.SaveChanges();
        }

    }
}