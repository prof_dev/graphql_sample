﻿using SampleApp.Core.Interfaces;
using SampleApp.Core.SharedKernel;
using SampleApp.Infrastructure.Data;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using StructureMap;

using Swashbuckle.AspNetCore.Swagger;

using System;

namespace SampleApp.Web
{
    using AutoMapper;

    using Microsoft.AspNetCore.Authorization;

    using SampleApp.Web.Authication;

    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(
                options =>
                    {
                        // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                        options.CheckConsentNeeded = context => true;
                        options.MinimumSameSitePolicy = SameSiteMode.None;
                    });

            services.AddAuthentication(
                    options =>
                        {
                            options.DefaultAuthenticateScheme = CustomAuthOptions.DefaultScheme;
                            options.DefaultChallengeScheme = CustomAuthOptions.DefaultScheme;
                        })
                // Call custom authentication extension method
                .AddCustomAuth(x => { });

            // TODO: Add DbContext and IOC
            string dbName = Guid.NewGuid().ToString();
            services.AddDbContext<AppDbContext>(options => options.UseInMemoryDatabase(dbName));
            //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc(
                options =>
                    {
                        options.Filters.Add(new CustomAuthFilter(new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build()));
                    }).AddControllersAsServices().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAutoMapper();

            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info { Title = "Sample WEB API", Version = "v1" }); });

            var container = new Container();

            container.Configure(
                config =>
                    {
                        config.Scan(
                            _ =>
                                {
                                    _.AssemblyContainingType(typeof(Startup)); // Web
                                    _.AssemblyContainingType(typeof(BaseEntity)); // Core
                                    _.Assembly("SampleApp.DomainLayer"); // DomainLayer
                                    _.Assembly("SampleApp.ServiceLayer"); // ServiceLayer
                                    _.Assembly("SampleApp.PresentationLayer"); // PresentationLayer
                                    _.WithDefaultConventions();
                                });

                        config.For(typeof(IRepository<>)).Add(typeof(EfRepository<>));

                        //Populate the container using the service collection
                        config.Populate(services);
                    });

            return container.GetInstance<IServiceProvider>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sample WEB API V1"); });

            app.PopulateTestData();

            app.UseMvc(
                routes =>
                    {
                        routes.MapRoute("auth", "auth/invoices", defaults: new {controller = "InvoicesAuth", action = "ListAuth" });
                        routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}");
                    });
        }
    }
}