﻿using SampleApp.Core.Entities;
using SampleApp.Core.Interfaces;
using SampleApp.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace SampleApp.Tests.Integration.Data
{
    using SampleApp.Core.Enums;

    public class EfRepositoryShould
    {
        private AppDbContext _dbContext;

        private static DbContextOptions<AppDbContext> CreateNewContextOptions()
        {
            // Create a fresh service provider, and therefore a fresh
            // InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            builder.UseInMemoryDatabase("SampleApp")
                   .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }

        [Fact]
        public void AddItemAndSetId()
        {
            var repository = GetRepository();
            var item = new InvoiceBuilder().Build();

            repository.Add(item);

            var newItem = repository.List().FirstOrDefault();

            Assert.Equal(item, newItem);
            Assert.True(newItem?.Id != Guid.Empty);
        }

        [Fact]
        public void UpdateItemAfterAddingIt()
        {
            // add an invoice
            var repository = GetRepository();
            var initialState = InvoiceState.Created;
            var item = new InvoiceBuilder().State(initialState).Build();

            repository.Add(item);

            // detach the item so we get a different instance
            _dbContext.Entry(item).State = EntityState.Detached;

            // fetch the item and update its state
            var newItem = repository.List()
                .FirstOrDefault(i => i.State == initialState);
            Assert.NotNull(newItem);
            Assert.NotSame(item, newItem);
            var newState = InvoiceState.PartlyPaid;
            newItem.State = newState;

            // Update the item
            repository.Update(newItem);
            var updatedItem = repository.List()
                .FirstOrDefault(i => i.State == newState);

            Assert.NotNull(updatedItem);
            Assert.NotEqual(item.State, updatedItem.State);
            Assert.Equal(newItem.Id, updatedItem.Id);
        }

        [Fact]
        public void DeleteItemAfterAddingIt()
        {
            // add an item
            var repository = GetRepository();
            var initialTitle = InvoiceState.Created;
            var item = new InvoiceBuilder().State(initialTitle).Build();
            repository.Add(item);

            // delete the item
            repository.Delete(item);

            // verify it's no longer there
            Assert.DoesNotContain(repository.List(),
                i => i.State == initialTitle);
        }

        private EfRepository<Invoice> GetRepository()
        {
            var options = CreateNewContextOptions();
            _dbContext = new AppDbContext(options);
            return new EfRepository<Invoice>(_dbContext);
        }
    }
}
