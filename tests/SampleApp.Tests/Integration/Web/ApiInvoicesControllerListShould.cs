﻿using SampleApp.Core.Entities;
using SampleApp.Web;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace SampleApp.Tests.Integration.Web
{
    using SampleApp.Core.Enums;

    public class ApiInvoicesControllerListShould : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public ApiInvoicesControllerListShould(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task ReturnsAllInvoices()
        {
            var response = await _client.GetAsync("/api/invoices");
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<IEnumerable<Invoice>>(stringResponse).ToList();

            Assert.Equal(3, result.Count());
            Assert.Equal(3, result.Count(a => a.State == InvoiceState.Created));
            Assert.Equal(0, result.Count(a => a.State != InvoiceState.Created));
        }
    }
}
