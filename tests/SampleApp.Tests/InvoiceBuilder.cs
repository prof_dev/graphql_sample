﻿using SampleApp.Core.Entities;

namespace SampleApp.Tests
{
    using System;
    using System.Collections.Generic;

    using SampleApp.Core.Enums;

    public class InvoiceBuilder
    {
        private readonly Invoice _invoice = new Invoice();

        public InvoiceBuilder Id(Guid id)
        {
            this._invoice.Id = id;
            return this;
        }

        public InvoiceBuilder State(InvoiceState state)
        {
            this._invoice.State = state;
            return this;
        }

        public InvoiceBuilder Items(IList<Item> items)
        {
            this._invoice.Items = items;
            return this;
        }

        public Invoice Build() => this._invoice;
    }
}
