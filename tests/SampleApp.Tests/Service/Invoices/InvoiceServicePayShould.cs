﻿namespace SampleApp.Tests.Service.Invoices
{
    using System;
    using System.Threading.Tasks;

    using Moq;

    using SampleApp.Core.Entities;
    using SampleApp.Core.Enums;
    using SampleApp.Core.Exceptions;
    using SampleApp.Core.Interfaces.Repositories;
    using SampleApp.Core.Interfaces.Services;
    using SampleApp.ServiceLayer;
    using SampleApp.Tests.Integration.Web;
    using SampleApp.Web;

    using Xunit;

    public class InvoiceServicePayShould : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly IInvoiceService service;

        private readonly Invoice invoice;

        public InvoiceServicePayShould()
        {

            this.invoice = new InvoiceBuilder().State(InvoiceState.Created).Build();
            var repo = new Mock<IInvoiceRepository>();
            repo.Setup(d => d.GetById(It.Is<Guid>(id => id == this.invoice.Id))).Returns(this.invoice);
            repo.Setup(d => d.Update(It.Is<Invoice>(invoice => true), true));

            this.service = new InvoiceService(repo.Object, null, null);
        }

        [Fact]
        public async Task ChangeInvoiceState()
        {
            Assert.Equal(this.invoice.State, InvoiceState.Created);

            this.service.Pay(this.invoice.Id);

            Assert.Equal(this.invoice.State,InvoiceState.Paid);
        }

        [Fact]
        public async Task ThrowExceptionIfPaid()
        {
            invoice.State = InvoiceState.Paid;
            Assert.Equal(this.invoice.State, InvoiceState.Paid);

            Assert.Throws<WrongInvoiceStateException>(()=>this.service.Pay(this.invoice.Id));
        }
    }
}
